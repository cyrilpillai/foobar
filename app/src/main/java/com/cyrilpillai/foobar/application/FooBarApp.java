package com.cyrilpillai.foobar.application;

import android.app.Application;
import android.content.Context;

import com.facebook.stetho.Stetho;


public class FooBarApp extends Application {
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        Stetho.initializeWithDefaults(context);
    }

    public static Context getContext() {
        return context;
    }
}
