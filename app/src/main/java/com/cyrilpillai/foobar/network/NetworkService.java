package com.cyrilpillai.foobar.network;

import com.cyrilpillai.foobar.database.entity.User;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface NetworkService {
    @GET("users/{username}")
    Call<User> getUserProfile(@Path("username") String username);
}
