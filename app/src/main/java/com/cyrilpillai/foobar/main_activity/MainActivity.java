package com.cyrilpillai.foobar.main_activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.Toast;

import com.cyrilpillai.foobar.R;
import com.cyrilpillai.foobar.application.FooBarApp;
import com.cyrilpillai.foobar.database.repo.UserRepo;
import com.cyrilpillai.foobar.database.entity.User;
import com.cyrilpillai.foobar.network.ApiClient;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private AppCompatEditText edtUserName;
    private AppCompatTextView tvTotalUserCount;
    private AppCompatButton btnSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edtUserName = (AppCompatEditText) findViewById(R.id.edt_user_name);
        tvTotalUserCount = (AppCompatTextView) findViewById(R.id.tv_total_user_count);
        btnSearch = (AppCompatButton) findViewById(R.id.btn_search);

        refreshTotalCountView();

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userName = edtUserName.getText().toString();
                if (!userName.isEmpty()) {
                    searchUserProfile(userName);
                }
            }
        });
    }

    private void refreshTotalCountView() {
        tvTotalUserCount.setText(String.format(Locale.getDefault(), getString(R.string.user_count),
                UserRepo.getInstance().getUsersCount()));
    }

    private void searchUserProfile(String username) {
        Call<User> call = ApiClient.getNetworkService().getUserProfile(username);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    UserRepo.getInstance().saveUserProfile(response.body());
                    refreshTotalCountView();
                    Toast.makeText(FooBarApp.getContext(), R.string.user_saved, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(FooBarApp.getContext(), R.string.user_not_found, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(FooBarApp.getContext(), R.string.error_occurred, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
