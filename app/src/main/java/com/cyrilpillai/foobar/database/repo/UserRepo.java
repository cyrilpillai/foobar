package com.cyrilpillai.foobar.database.repo;

import com.cyrilpillai.foobar.dao.UserDao;
import com.cyrilpillai.foobar.database.entity.User;
import com.cyrilpillai.foobar.database.helpers.BaseRepo;

import java.util.List;

public class UserRepo extends BaseRepo {

    private static UserRepo instance = null;
    private UserDao dao;

    private UserRepo() {
        super();
        dao = daoSession.getUserDao();
    }

    public static UserRepo getInstance() {
        if (instance == null) {
            instance = new UserRepo();
        }
        return instance;
    }

    public void saveUserProfile(User user) {
        dao.insertOrReplace(user);
    }

    public void saveUserProfileList(List<User> users) {
        dao.insertOrReplaceInTx(users);
    }

    public User getUserProfile(String username) {
        List<User> users = dao.queryBuilder().where(UserDao.Properties.Name.eq(username)).list();
        return (users != null && users.size() > 0) ? users.get(0) : null;
    }

    public List<User> getAllUserProfiles() {
        return dao.queryBuilder().list();
    }

    public int getUsersCount() {
        return getAllUserProfiles().size();
    }
}
